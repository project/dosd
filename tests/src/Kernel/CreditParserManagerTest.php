<?php

namespace Drupal\Tests\dosd\Kernel;

use Drupal\dosd\Entity\CreditEntity;
use Drupal\KernelTests\KernelTestBase;

/**
 * Test for plugin.manager.dosd.parser.
 *
 * @coversDefaultClass \Drupal\dosd\CreditParsersManager
 */
class CreditParserManagerTest extends KernelTestBase {

  /**
   * The service under test.
   *
   * @var \Drupal\dosd\CreditParsersManager
   */
  protected $parserManager;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = ['dosd', 'dosd_test', 'user'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('credit_entity');
    $this->installEntitySchema('user');

    $this->parserManager = \Drupal::service('plugin.manager.dosd.parser');
  }

  /**
   * @covers ::collectCredits
   */
  public function testCollectCredits() {
    $company = NULL;
    $user = 'test';
    $start_date = 'first day of last month';
    $end_date = 'last day of last month';

    $this->parserManager->collectCredits($company, $user, $start_date, $end_date);
    $credits = CreditEntity::loadMultiple();
    foreach ($credits as $credit) {
      $link = $credit->get('link')->getString();
      $this->assertEqual($link, 'https://drupal.org/i/fakeid');
    }
  }

}
