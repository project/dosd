<?php

namespace Drupal\dosd_test\Plugin\dosd\CreditParser;

use Drupal\dosd\Entity\CreditEntity;
use Drupal\dosd\Plugin\dosd\CreditParser\CreditParserBase;

/**
 * Provides a 'FakeCreditParser' plugin.
 *
 * @CreditParser(
 *   id = "FakeCreditParser",
 *   label = @Translation("FakeCreditParser"),
 * )
 */
class FakeCreditParser extends CreditParserBase {

  /**
   * {@inheritdoc}
   */
  public function getCredits(): array {
    $fake_credit = CreditEntity::create([
      'link' => 'https://drupal.org/i/fakeid',
      'uid' => '1',
    ]);
    return [$fake_credit];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return [];
  }

}
