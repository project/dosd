## INSTALLATION
- Install the Drupal Open Source Dashboard module as you would normally install a contributed Drupal module. 
- Visit <https://www.drupal.org/docs/extending-drupal/installing-modules> for further information.
## CONFIGURATION
No configuration is needed.
## USAGE
Run drush dosd:parse 2022/10/14 2022/10/22 2114867 dorgusername
- 2022/10/14 - start date.
- 2022/10/22 - end date.
- 2114867 - company id on Drupal.org (or Null to check only user's credit. See the next)
- dorgusername - add D.org Username to check only user's credit (optional)
- Visit http://yourdomain.com/dosd to view collected credits.

Run drush dosd:parse 'first day of last month' 'last day of last month' company_id
- 'first day of last month' - as a start date could be used sting date notations, available for strtotime PHP function.
- 'last day of last month' - as an end date could be used sting date notations, available for strtotime PHP function.
- These options are useful to use in cron run.

Run drush dosd:parse --interactive=1
- This will use interactive shell to ask for the input parameters
