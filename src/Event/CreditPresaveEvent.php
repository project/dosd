<?php

namespace Drupal\dosd\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\dosd\Entity\CreditEntityInterface;

/**
 * Event that is fired before.
 */
class CreditPresaveEvent extends Event {

  public const PRE_SAVE = 'dosd.credit_pre_save';

  /**
   * The Credit.
   *
   * @var \Drupal\dosd\Entity\CreditEntityInterface
   */
  protected CreditEntityInterface $credit;

  /**
   * Indicates if we should save the credit.
   *
   * @var bool
   *   TRUE if the credit should be saved.
   */
  protected bool $shouldSave = TRUE;

  /**
   * Constructs the object.
   *
   * @param \Drupal\dosd\Entity\CreditEntityInterface $credit
   *   Credit entity.
   */
  public function __construct(CreditEntityInterface $credit) {
    $this->credit = $credit;
  }

  /**
   * Returns credit.
   *
   * @return \Drupal\dosd\Entity\CreditEntityInterface
   *   The Credit.
   */
  public function getCredit() {
    return $this->credit;
  }

  /**
   * Sets the credit.
   *
   * @param \Drupal\dosd\Entity\CreditEntityInterface $credit
   *   The Credit.
   */
  public function setCredit(CreditEntityInterface $credit) {
    $this->credit = $credit;
  }

  /**
   * Sets ShouldSave marker to FALSE.
   */
  public function doNotSave() {
    $this->shouldSave = FALSE;
  }

  /**
   * Returns ShouldSave marker.
   *
   * @return bool
   *   ShouldSave value.
   */
  public function checkShouldSave() {
    return $this->shouldSave;
  }

}
