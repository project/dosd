<?php

namespace Drupal\dosd\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Inteface for custom credit types implementation.
 */
interface CreditEntityInterface extends ContentEntityInterface {

  /**
   * Checks if the credit is already in DB.
   *
   * @return bool
   *   True if credit is already stored in the DB.
   */
  public function isStored();

}
