<?php

namespace Drupal\dosd\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Provides entity for creadit storing data.
 *
 * @package Drupal\dosd\Entity
 *
 * @ContentEntityType(
 *   id = "credit_entity",
 *   label = @Translation("Drupal credit entity"),
 *   base_table = "credit_entity",
 *   entity_keys = {
 *     "id" = "cid",
 *   },
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 * )
 */
class CreditEntity extends ContentEntityBase implements CreditEntityInterface {

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = [];

    $fields[$entity_type->getKey('id')] = BaseFieldDefinition::create('integer')
      ->setLabel(t('cid'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $fields['link'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Link to the issue/commit'))
      ->setDescription(t('Link to the issue/commit'));

    $fields['date'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Received on'))
      ->setDescription(t('The time that the credit was received'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE);

    $fields['type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Type'))
      ->setDescription(t('Type of the credit'));

    $fields['description'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Description'))
      ->setDescription(t('Description of the credit'));

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setSetting('target_type', 'user')
      ->setTranslatable($entity_type->isTranslatable())
      ->setDefaultValueCallback(static::class . '::getDefaultEntityOwner')
      ->setLabel(t('Received by'))
      ->setDescription(t('The user id for the credit'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE);

    return $fields;
  }

  /**
   * Checks if the credit is already in DB.
   *
   * @return bool
   *   True if credit is already stored in the DB.
   */
  public function isStored() {

    $storage = $this->entityTypeManager()->getStorage('credit_entity');

    $ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('link', $this->get('link')->getString())
      ->condition('type', $this->get('type')->getString())
      ->condition('uid', $this->get('uid')->getString())
      ->execute();

    return (bool) $ids;

  }

}
