<?php

namespace Drupal\dosd;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\dosd\Event\CreditPresaveEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\dosd\Annotation\CreditParser;

/**
 * Manages dosd contribution parser plugins.
 */
class CreditParsersManager extends DefaultPluginManager {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs a new DrupalOpenSourceDashboardContributionParsersManager.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler for the alter hook.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The Event Dispatcher.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config Factory injection.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler,
    EventDispatcherInterface $event_dispatcher
  ) {
    parent::__construct(
      'Plugin/dosd/CreditParser',
      $namespaces,
      $module_handler,
      CreditParserPluginInterface::class,
      CreditParser::class
    );

    $this->eventDispatcher = $event_dispatcher;
    $this->alterInfo('dosd_credit_parser_info');
    $this->setCacheBackend($cache_backend, 'dosd_credit_parser_plugins');
  }

  /**
   * Collects all credits from parsers.
   */
  public function collectCredits($company, $user, $start_date, $end_date) {
    $plugin_credits = [];
    $settings = [
      'company' => $company,
      'user' => $user,
      'start_date' => strtotime($start_date),
      'end_date' => strtotime($end_date),
    ];

    foreach (array_keys($this->getDefinitions()) as $plugin_id) {
      /** @var \Drupal\dosd\CreditParserPluginInterface $plugin */
      $plugin = $this->createInstance($plugin_id, $settings);
      $plugin_credits[$plugin_id] = $plugin->getCredits();
    }
    $credits = array_merge([], ...array_values($plugin_credits));

    foreach ($credits as &$credit) {
      /** @var \Drupal\dosd\Entity\CreditEntityInterface $credit */
      $event = new CreditPresaveEvent($credit);
      $this->eventDispatcher->dispatch($event, CreditPresaveEvent::PRE_SAVE);

      if ($event->checkShouldSave()) {
        $credit->save();
      }

    }
  }

}
