<?php

namespace Drupal\dosd\Plugin\dosd\CreditParser;

use Drupal\dosd\Entity\CreditEntity;

/**
 * Provides a 'CreditParserDorg' plugin.
 *
 * @CreditParser(
 *   id = "CreditParserDorg",
 *   label = @Translation("CreditParserDorg"),
 * )
 */
final class CreditParserDorg extends CreditParserBase {
  private const COMMENTS_ENDPOINT = 'comment.json';
  private const NODE_ENDPOINT = 'node.json';
  private const USER_ENDPOINT = 'user.json';
  private const CONTRIB_TYPE = 'credit';
  private const ISSUE_STATUS_FIXED = '2';
  private const CREDIT_FOUND = '+';
  private const CREDIT_NOT_FOUND = '-';
  private const ISSUE_STATUS_CLOSED_FIXED = '7';

  /**
   * {@inheritdoc}
   */
  public function getCredits():array {
    $comments = [];
    if (!empty($this->company)) {
      $comments_by_company = $this->getCommentsByCompany($this->company);
      $comments_by_customer = $this->getCommentsByCustomer($this->company);
      $comments = array_merge($comments_by_company, $comments_by_customer);
    }
    if (!empty($this->user)) {
      $comments = $this->getCommentsByUser($this->user);
    }
    $comments_credited = [];
    $this->logger->info('Comments gathered (' . count($comments) . ')');
    foreach ($comments as $comment) {
      if (!$comment || empty($comment['node'])) {
        continue;
      }
      $issue = $this->getNode($comment['node']['id'])['list'][0];

      if (!isset($issue['field_project'])) {
        // The comment isn't supported yet. Examples: documentation edits.
        continue;
      }

      $project = $this->getNode($issue['field_project']['id'])['list'][0];
      $dorg_user = $this->getUser($comment['author']['id'])['list'][0];
      if (empty($issue['field_issue_credit'])) {
        echo self::CREDIT_NOT_FOUND;
        continue;
      }
      if (!$this->inTimeFrame($issue['changed'])) {
        echo self::CREDIT_NOT_FOUND;
        continue;
      }

      if ($issue['field_issue_status'] !== self::ISSUE_STATUS_CLOSED_FIXED && $issue['field_issue_status'] !== self::ISSUE_STATUS_FIXED) {
        echo self::CREDIT_NOT_FOUND;
        continue;
      }
      echo self::CREDIT_FOUND;

      $is_credited = array_filter($issue['field_issue_credit'],
        static function ($v, $k) use ($comment) {
          return $v['id'] === $comment['cid'];
        }, ARRAY_FILTER_USE_BOTH);
      if ($is_credited) {
        $data = $this->prepareResponse($issue, $project, $dorg_user);

        $user = $this->processUser($dorg_user['uid'], $dorg_user['name'], $dorg_user['url'], strtolower($dorg_user['url']));
        $data['uid'] = $user->id();

        $comments_credited[] = $this->createCreditEntity($data);
      }
    }
    $this->logger->info('Credits gathered ' . count($comments_credited));
    return $comments_credited;
  }

  /**
   * Query Drupal API for user's comments.
   *
   * @param string $user
   *   Username.
   *
   * @return array
   *   API response.
   */
  private function getCommentsByUser($user): array {
    $params = [
      'name' => $user,
      'sort' => 'created',
      'direction' => 'desc',
      'page' => 0,
    ];
    return $this->getComments($params);
  }

  /**
   * Query Drupal API for company's comments.
   *
   * @param string $company
   *   Company name.
   *
   * @return array
   *   API response.
   */
  private function getCommentsByCompany($company) {
    $params = [
      'field_attribute_contribution_to' => $company,
      'sort' => 'created',
      'direction' => 'desc',
      'page' => 0,
    ];
    return $this->getComments($params);
  }

  /**
   * Query Drupal API for customer's comments.
   *
   * @param string $customer
   *   Customer name.
   *
   * @return array
   *   API response.
   */
  private function getCommentsByCustomer($customer): array {
    $params = [
      'field_for_customer' => $customer,
      'sort' => 'created',
      'direction' => 'desc',
      'page' => 0,
    ];
    return $this->getComments($params);
  }

  /**
   * Query Drupal API for specific node.
   *
   * @param mixed $issue_id
   *   Drupal.org issue id.
   *
   * @return array|mixed
   *   API response.
   */
  private function getNode($issue_id) {
    return $this->makeRequest(self::NODE_ENDPOINT, ['nid' => $issue_id]);
  }

  /**
   * Query Drupal API for specific user.
   *
   * @param mixed $user_id
   *   User ID.
   *
   * @return array|mixed
   *   API response.
   */
  private function getUser($user_id) {
    return $this->makeRequest(self::USER_ENDPOINT, ['uid' => $user_id]);
  }

  /**
   * Helper to map dosd credit entity with gathered info.
   *
   * @param array $issue
   *   Array with issue info.
   * @param array $project
   *   Array with project info.
   * @param array $user
   *   Array with user info.
   *
   * @return array
   *   Issue credit info.
   */
  private function prepareResponse(array $issue, array $project, array $user): array {

    return [
      'user_email' => strtolower($user['url']),
      'user_name' => $user['name'],
      'user_country' => is_array($user['field_country']) ? reset($user['field_country']) : $user['field_country'],
      'user_url' => $user['url'],
      'user_fio' => $user['field_first_name'] . ' ' . $user['field_last_name'],
      'contrib_url' => $issue['url'] ?? '',
      'contrib_date' => $issue['changed'] ?? '',
      'contrib_type' => self::CONTRIB_TYPE,
      'contrib_description' => $project['title'],
    ];
  }

  /**
   * Recursive function to collect all comments.
   *
   * @param array $params
   *   Query params.
   * @param array $response
   *   Gathered response.
   *
   * @return array
   *   Array of comments gathered.
   */
  private function getComments(array $params, array $response = []): array {
    if ($this->verbose) {
      $this->logger->info(' Parsing comments API, page ' . $params['page']);
    }
    $comments = $this->makeRequest(self::COMMENTS_ENDPOINT, $params);
    $response = array_merge($response, $comments['list']);
    if ($comments['self'] === $comments['last']) {
      return $response;
    }
    $last_comment_date = end($comments['list'])['created'];
    if ($last_comment_date < $this->startDate) {
      return $response;
    }
    $params['page']++;
    return $this->getComments($params, $response);
  }

  /**
   * Helper to create CreditEntity.
   *
   * @param array $data
   *   Credit data array.
   *
   * @return \Drupal\dosd\Entity\CreditEntity
   *   Created CreditEntity
   */
  private function createCreditEntity(array $data): CreditEntity {
    return CreditEntity::create([
      'link' => $data['contrib_url'],
      'date' => $data['contrib_date'],
      'type' => $data['contrib_type'],
      'description' => $data['contrib_description'],
      'uid' => $data['uid'],
      'status' => 1,
    ]);
  }

}
