<?php

namespace Drupal\dosd\Plugin\dosd\CreditParser;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\UserStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\dosd\CreditParserPluginInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;
use JsonException;

/**
 * Provides a base class for dosd credit parser plugins.
 *
 * @see \Drupal\dosd\CreditParserPluginInterface
 * @see \Drupal\dosd\Annotation\CreditParser
 * @see \Drupal\dosd\CreditParsersManager
 */
abstract class CreditParserBase extends PluginBase implements CreditParserPluginInterface, ContainerFactoryPluginInterface {

  protected LoggerChannelInterface $logger;

  private ClientInterface $client;

  protected mixed $company;

  protected mixed $user;

  protected mixed $startDate;

  protected mixed $endDate;

  protected bool $verbose;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * @var \Drupal\user\UserStorageInterface
   */
  protected UserStorageInterface $userStorage;

  /**
   * {@inheritdoc}
   */
  abstract public function getCredits():array;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    LoggerChannelFactoryInterface $loggerFactory,
    ClientInterface $http_client,
    EntityTypeManagerInterface $entity_type_manager,
    UserStorageInterface $user_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->logger = $loggerFactory->get('dosd');
    $this->client = $http_client;
    $this->entityTypeManager = $entity_type_manager;
    $this->company = $configuration['company'];
    $this->user = $configuration['user'];
    $this->startDate = $configuration['start_date'];
    $this->endDate = $configuration['end_date'];
    $this->verbose = TRUE;
    $this->userStorage = $user_storage;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('http_client'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('user')
    );
  }

  /**
   * Wrappper to get requests to Drupal APIs.
   *
   * @param string $uri
   *   URI to request.
   * @param array $params
   *   HTTP request params.
   * @param array|null $headers
   *   HTTP request headers.
   *
   * @return array|mixed
   *   API request results.
   */
  public function makeRequest(string $uri, array $params, array $headers = NULL): mixed {
    $results = [];
    try {
      $response = $this->client->request('GET', $uri, [
        'query' => $params,
        'headers' => $headers,
        'base_uri' => self::BASE_ENDPOINT,
      ]);
      if ($response->getStatusCode() !== 200) {
        return $results;
      }
      $body = $response->getBody();
      $results = json_decode($body, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    catch (ClientException | GuzzleException | JsonException $e) {
      $this->logger->error($e->getMessage());
    }
    return $results;
  }

  /**
   * Helper to check whether comment in desired timeframe.
   *
   * @param int $comment_date
   *   Comment date.
   *
   * @return bool
   *   TRUE if a comment within timeframe.
   */
  protected function inTimeFrame(int $comment_date): bool {
    return ($comment_date >= $this->startDate && $comment_date <= $this->endDate);
  }

  /**
   * Loads/Creates/Updates a local user with Drupal.Org data.
   *
   * @param int $dorg_uid
   *   Drupal.Org UID.
   * @param string $name
   *   Drupal.Org username.
   * @param string $url
   *   Drupal.Org user profile URL.
   * @param string $email
   *   Email to store in the DB.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function processUser($dorg_uid, $name, $url, $email): ?EntityInterface {
    $uid = $this->userLookup($dorg_uid, $name, $url);
    $user_storage = $this->entityTypeManager->getStorage('user');

    if ($uid) {
      $user = $user_storage->load($uid);
    }
    else {
      $user = $user_storage->create([
        'name' => $name,
        'mail' => $email,
      ]);
    }

    $user->set('dorg_username', $name);
    $user->set('dorg_link', $url);
    $user->set('dorg_uid', $dorg_uid);
    $user->save();

    return $user;
  }

  /**
   * Look up for user in the DB.
   *
   * @param int $dorg_uid
   *   Drupal.Org UID.
   * @param string $name
   *   Drupal.Org username.
   * @param string $url
   *   Drupal.Org user profile URL.
   *
   * @return int|null
   *   Drupal UID.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function userLookup($dorg_uid, $name, $url) {
    $storage = $this->entityTypeManager->getStorage('user');

    // Try Drupal.org UID first.
    $user = $storage->loadByProperties([
      'dorg_uid' => $dorg_uid,
    ]);
    if ($user) {
      return reset($user)->id();
    }

    $user = $storage->loadByProperties([
      'dorg_username' => $name,
      'dorg_link' => $url,
    ]);
    if ($user) {
      return reset($user)->id();
    }

    $user = $storage->loadByProperties([
      'name' => $name,
    ]);
    if ($user) {
      return reset($user)->id();
    }

    return NULL;
  }

  public function getCacheContexts() {
    // TODO: Implement getCacheContexts() method.
  }

  public function getCacheTags() {
    // TODO: Implement getCacheTags() method.
  }

  public function getCacheMaxAge() {
    // TODO: Implement getCacheMaxAge() method.
  }

}
