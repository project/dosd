<?php

namespace Drupal\dosd;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\CacheableDependencyInterface;

/**
 * Provides an interface for a plugin for a contribution parser.
 *
 * Plugins of this type need to be annotated with
 * \Drupal\dosd\Annotation\ContributionParser annotation, and placed in the
 * Plugin\ContributionParser namespace directory. They are managed by the
 * \Drupal\dosd\DrupalOpenSourceDashboardContributionParsersManager
 * plugin manager class. There is a base class that may be helpful:
 * \Drupal\dosd\Plugin\ContributionParser\ContributionParserPluginBase.
 */
interface CreditParserPluginInterface extends PluginInspectionInterface, CacheableDependencyInterface {

  public const BASE_ENDPOINT = 'https://www.drupal.org/api-d7/';

  /**
   * Returns the list of credits collected. They will be saved later.
   *
   * @return \Drupal\dosd\Entity\CreditEntityInterface[]
   *   The array of Credit entities to be saved.
   */
  public function getCredits(): array;

}
