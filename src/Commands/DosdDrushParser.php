<?php

namespace Drupal\dosd\Commands;

use Drush\Commands\DrushCommands;
use Drupal\Dosd\CreditParsersManager;

/**
 * A drush command file.
 *
 * @package Drupal\dosd\Commands
 */
class DosdDrushParser extends DrushCommands {

  /**
   * @var \Drupal\Dosd\CreditParsersManager
   */
  private CreditParsersManager $parserManager;

  /**
   * DosdDrushParser constructor.
   */
  public function __construct(CreditParsersManager $parserManager) {
    parent::__construct();
    $this->parserManager = $parserManager;
  }

  /**
   * Drush command that displays the given text.
   *
   * @param string|null $start_date
   *   Start date.
   * @param string|null $end_date
   *   End date.
   * @param string|null $company
   *   Company ID.
   * @param string|null $user
   *   User Name.
   * @param array $options
   *   Command options.
   *
   * @options interactive use interactive shell?
   *
   * @command dosd:parse
   * @aliases dosdp
   *
   * @usage drush dosd:parse 2022/10/14 2022/10/22 2114867
   *   Parse credits in the given time interval for the company with id 2114867
   * @usage drush dosd:parse 2022/10/14 2022/10/22 NULL username
   *   Parse credits in the given time interval for the user username
   * @usage drush dosd:parse --interactive=1
   *   Enter command params in interactive way
   */
  public function parse(
    string $start_date = NULL,
    string $end_date = NULL,
    string $company = NULL,
    string $user = NULL,
    array $options = [
      'interactive' => 'FALSE',
    ]
  ): void {
    $is_interactive = strtolower($options['interactive']);
    if ($is_interactive === 'true'
      || $is_interactive === '1'
      || $is_interactive === 'y'
      || $is_interactive === 'yes') {
      $company = $this->io()->ask('Enter the Drupal.org company id (could be skipped in favour of user) ...');
      $user = $this->io()->ask('Enter the Drupal.org user name (could be skipped in favour of company) ...');
      $start_date = $this->io()->ask('Enter the start date (YYYY/MM/DD) ...');
      $end_date = $this->io()->ask('Enter the end date (YYYY/MM/DD) ...');
    }

    $this->output()->writeln('Company - ' . $company);
    $this->output()->writeln('User - ' . $user);
    $this->output()->writeln('Start date - ' . $start_date);
    $this->output()->writeln('End date - ' . $end_date);

    $this->parserManager->collectCredits($company, $user, $start_date, $end_date);
  }

}
