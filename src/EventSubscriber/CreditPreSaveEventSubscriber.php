<?php

namespace Drupal\dosd\EventSubscriber;

use Drupal\dosd\Event\CreditPresaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class for Credit pre-save events.
 */
class CreditPreSaveEventSubscriber implements EventSubscriberInterface {

  /**
   * Removes Credit if already collected.
   */
  public function filterDuplicates(CreditPresaveEvent $event) {
    $credit = $event->getCredit();

    if ($credit->isStored()) {
      $event->doNotSave();
    }

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      CreditPresaveEvent::PRE_SAVE => 'filterDuplicates',
    ];
  }

}
